<div id="welcome">
	<div class="row">
		<div class="container clearfix">
			<div class="wlcLeft col-6 fl">
				<div class="welcome-text">
					<h4>welcome to</h4>
					<h1>Haul-R-Us Junk Removal LLC</h1>
					<h5>We are Family owned and operated, based out of Fresno California </h5>
					<p>Our philosophy is simple; provide exceptional customer service, a service of great value, at a reasonable price, with the highest level of integrity and all while acting green responsible towards our environment. In addition, we also follow up and follow through!</p>
					<p>Our innovative approach to getting rid of your entire unwanted junk, whether at home or in the office is just a phone call or click away.  Call Now for a free, no obligation estimate at 559.282.0002</p>
					<a href="#" class="btn2">GET A FREE QUOTE</a>
					<p class="phone fr">CALL US TODAY <span><?php $this->info(["phone","tel"]);?></span> </p>
				</div>
				</div>
			<div class="wlcRight col-6 fr">
				<img src="public/images/content/box.png" alt="box" class="box">
				<img src="public/images/content/welcome.jpg" alt="truck" class="truck">
			</div>
		</div>
	</div>
</div>
<div id="services">
	<div class="row">
		<div class="container clearfix">
			<div class="svcLeft col-6 fl">
				<img src="public/images/content/services.jpg" alt="dump-truck" class="dump-truck">
			</div>
			<div class="svcRight col-6 fr">
				<div class="container">
					<h2>SERVICES OFFERED</h2>
					<ul>
						<li>Mattress Removal</li>
						<li>Junk Removal</li>
						<li>Yard Debris Removal</li>
						<li>Appliance Removal</li>
						<li>Furniture Removal</li>
						<li>Recycling</li>
						<li>Christmas Tree Removal</li>
						<li>Electronic Waste Removal</li>
						<li>Storm Debris Removal</li>
						<li>Residential Services</li>
						<li>Post Construction Cleaning</li>
						<li>Cleanouts</li>
						<li>Commercial Services</li>
						<li>Property Cleanout</li>
						<li>Eviction Cleanout</li>
						<li>Foreclosure Cleanouts</li>
						<li>Carpet Removal</li>
						<li>Light Demolition</li>
						<li>Boat/Rv Trailer Removal</li>
						<li>Hoarder Situation</li>
					</ul>
					<a href="services#content" class="btn">READ MORE</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="wedo">
	<div class="row">
		<h2 class="text-left">WHAT WE DO</h2>
		<div class="container">
			<div class="wedoIMG"> <img src="public/images/content/wedoIMG1.jpg" alt="WHAT WE DO IMAGE 1">	<p>JUNK REMOVAL</p> </div>
			<div class="wedoIMG"> <img src="public/images/content/wedoIMG2.jpg" alt="WHAT WE DO IMAGE 2">	<p>MATTRESS REMOVAL</p> </div>
			<div class="wedoIMG"> <img src="public/images/content/wedoIMG3.jpg" alt="WHAT WE DO IMAGE 3">	<p>TUB REMOVAL</p> </div>
			<div class="wedoIMG"> <img src="public/images/content/wedoIMG4.jpg" alt="WHAT WE DO IMAGE 4">	<p>APPLIANCE REMOVAL</p> </div>
			<div class="wedoIMG"> <img src="public/images/content/wedoIMG5.jpg" alt="WHAT WE DO IMAGE 5">	<p>EVICTION/FORECLOSURE CLEANOUTS</p> </div>
			<div class="wedoIMG"> <img src="public/images/content/wedoIMG6.jpg" alt="WHAT WE DO IMAGE 6">	<p>BOAT/TRAILER REMOVAL</p> </div>
			<div class="wedoIMG"> <img src="public/images/content/wedoIMG7.jpg" alt="WHAT WE DO IMAGE 7">	<p>LIGHT DEMOLITION</p> </div>
			<div class="wedoIMG"> <img src="public/images/content/wedoIMG8.jpg" alt="WHAT WE DO IMAGE 8">	<p>FURNITURE REMOVAL</p> </div>
		</div>
		<a href="services#content" class="btn text-left">VIEW MORE</a>
	</div>
</div>
<div id="reviews">
	<div class="row">
		<div class="container clearfix">
			<div class="rvwLeft col-6 fl">
				<img src="public/images/content/rvwIMG.jpg" alt="review" class="rvwIMG">
			</div>
			<div class="rvwRight col-6 fr">
				<div class="container">
					<h2>CLIENT REVIEWS</h2>
					<div class="text">
						<dl>
							<dt><img src="public/images/content/stars.jpg" alt="star"> -Leonard G.</dt>
							<dd> Your guys just left a little while ago. They were on time, efficient and did a good job for a reasonable rate. They also moved a 4’ dresser and a 5’ executive desk out of the house. All of it done at a great rate. I will recommend you to my friends. Thanks for your service.</dd>
						</dl>
						<dl>
							<dt><img src="public/images/content/stars.jpg" alt="star"> -Lynn H.</dt>
							<dd>Prompt communication, and was able to come out within 2 hours of contacting them. The three gentlemen that arrived were polite, friendly, and asked questions before they did the job to make sure they were doing what was requested. And if that wasn’t enough, they charged about half of another quote I got. I highly recommend them!!</dd>
						</dl>
					</div>
					<a href="reviews#content" class="btn">READ MORE</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="gallery">
	<div class="row">
		<h2>OUR GALLERY</h2>
		<div class="container">
			<img src="public/images/content/galleryIMG1.jpg" alt="Gallery Image 1" class="galleryIMG">
			<img src="public/images/content/galleryIMG2.jpg" alt="Gallery Image 2" class="galleryIMG">
			<img src="public/images/content/galleryIMG3.jpg" alt="Gallery Image 3" class="galleryIMG">
		</div>
		<a href="gallery#content" class="btn">VIEW OUR GALLERY</a>
	</div>
</div>
<div id="contact">
	<div class="row">
		<h2>Contact Us</h2>
		<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
			<div class="cntTop">
				<label><span class="ctc-hide">Name</span>
					<input type="text" name="name" placeholder="Name:">
				</label>
				<label><span class="ctc-hide">Phone</span>
					<input type="text" name="phone" placeholder="Phone:">
				</label>
				<label><span class="ctc-hide">Email</span>
					<input type="text" name="email" placeholder="Email:">
				</label>
			</div>
			<label><span class="ctc-hide">Message</span>
				<textarea name="message" cols="30" rows="10" placeholder="Message / Questions:"></textarea>
			</label>
			<div class="cntBot">
				<div class="left">
					<label>
						<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
					</label><br>
					<?php if( $this->siteInfo['policy_link'] ): ?>
					<label>
						<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
					</label>
					<?php endif ?>
				</div>
				<div class="right">
					<label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label>
					<div class="g-recaptcha"></div>
				</div>
			</div>
			<button type="submit" class="ctcBtn btn" disabled>SUBMIT FORM</button>
		</form>
	</div>
</div>
<div id="map">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d204504.2856716982!2d-119.93464442400285!3d36.78545132327649!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80945de1549e4e9d%3A0x7b12406449a3b811!2sFresno%2C+CA%2C+USA!5e0!3m2!1sen!2sph!4v1540449066817" allowfullscreen></iframe>
</div>
